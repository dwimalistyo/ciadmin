<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Management
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Delete User</h3>
            </div>
            
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="view_user" method="post">
              <div class="box-body">
                  
                <div class="form-group">
                  <label for="exampleInputFirstName">First Name</label>
                  <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $result['first_name'] ?>" placeholder="First Name" readonly>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputLastName">Last Name</label>
                  <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $result['last_name'] ?>" placeholder="Last Name" readonly>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputUserName">Username</label>
                  <input type="text" class="form-control" id="username" name="username" value="<?php echo $result['username'] ?>" placeholder="Username" readonly>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputPassword">Password</label>
                  <input type="text" class="form-control" id="password" name="password" value="<?php echo $result['password'] ?>" placeholder="Password" readonly>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail">Email</label>
                  <input type="email" class="form-control" id="email" name="email" value="<?php echo $result['email'] ?>" placeholder="Email" readonly>
                </div>
                
                </div>
              
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="cancel" name="cancel" value ="Cancel" class="btn btn-danger ">
                <input type="delete" name="delete" value ="Delete" class="btn btn-primary pull-right">
                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
              </div>
              
             
                
            </form>
            
          </div>
          <!-- /.box -->

          
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>  