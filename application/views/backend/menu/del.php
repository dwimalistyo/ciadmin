<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Management
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User Management</a></li>
        <li class="active">Del</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Del User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <?php
              
              if (isset($data))
              {
	
              }
              else {
                $data=0;
              }
              
              echo form_open();?>

              <div class="box-body">
              
                <div class="form-group">
                  <label for="inputUsername3" class="col-sm-2 control-label">Username</label>

                  <div class="col-sm-10">
                    <?php echo form_input('username', set_value('username', $data['username'])); ?>
                    <!-- <input type="text" class="form-control" id="inputEmail3" name="username" placeholder="Username"> -->
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <?php echo form_input('password', set_value('password', $data['password'])); ?>
                    <!-- <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">-->
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="inputPassword4" class="col-sm-2 control-label">Password [ReType]</label>

                  <div class="col-sm-10">
                    <?php echo form_input('password2', set_value('password2', $data['password2'])); ?>
                    <!-- <input type="password" class="form-control" id="inputPassword3" placeholder="Password"> -->
                  </div>
                </div>  
                  
                <div class="form-group">
                  <label for="inputFirstname3" class="col-sm-2 control-label">First Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="First Name">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="inputLastname3" class="col-sm-2 control-label">Last Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Last Name">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputCellphone3" class="col-sm-2 control-label">Cell Phone</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Cell Phone">
                  </div>
                </div>
                  
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                  </div>
                </div>
              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <?php echo form_submit('submit','SAVE'); ?>
                <!-- <button type="submit" class="btn btn-info pull-right">Submit</button> -->
                <button type="submit" class="btn btn-danger">Cancel</button>
                
              </div>
              <!-- /.box-footer -->
            <?php echo form_close();?>

            </form>
          </div>
          <!-- /.box -->
          
    </section>
    <!-- /.content -->
  </div>
  