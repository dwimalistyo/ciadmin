
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Management
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User Management</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Member</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>FirstName</th>
                  <th>LastName</th>
                  <th>Group</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($result as $row)
                {
                  echo   "<tr><td>";
                  
                  echo   $row->id;
                  echo   "</td><td>";
                  
                  echo   $row->username;
                  echo   "</td><td>";
                  
                  echo   $row->email;
                  echo   "</td><td>";
                  
                  
                  echo   $row->first_name;
                  echo   "</td><td>";
                  
                  echo   $row->last_name;
                  echo   "</td><td>";
                  
                  echo   "Admin";
                  echo   "</td><td>";
                  
                  echo   "<a href='".base_url('/user/view/')."".$row->id."'><small class=\"label bg-blue\">view</small></a>
                          <a href='".base_url('/user/edit/')."".$row->id."'><small class=\"label bg-yellow\">edit</small></a>
                          <a href='".base_url('/user/del/')."".$row->id."'><small class=\"label bg-red\">del</small></a>";
                  echo   "</td></tr>";
                  
                    
                }
                
                ?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>FirstName</th>
                  <th>LastName</th>
                  <th>Group</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
