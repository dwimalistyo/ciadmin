
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/plugins/datatables/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/plugins/fastclick/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/dist/js/app.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('/assets/AdminLTE-2.3.7/dist/js/demo.js') ?>"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
