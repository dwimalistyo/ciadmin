 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright 2016 <a href="http://www.ciptaaplikasi.com">Cipta Aplikasi</a>.</strong> All rights
    reserved.
  </footer>