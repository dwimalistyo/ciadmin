<?php

class User_model extends CI_Model {
    
    public $db_table = 'users';
    
    public function get_user(){
        
        $data = $this->db->select('*')
                     ->get($this->db_table);
        
        
        return $data->result();
        
    }
    
    
     public function add_user(
                        $ip,
                        $username,
                        $password,
                        $salt,
                        $email,
                        $activation_code,
                        $forgotten_password_code,
                        $forgotten_password_time,
                        $remember_code,
                        $created_on,
                        $last_login,
                        $active,
                        $first_name, 
                        $last_name,
                        $company,   
                        $phone
                                ) 
    {
        
        
        $data = array(
                        'ip_address'                => $ip,
                        'username'                  => $username,
                        'password'                  => $password,
                        'salt'                      => $salt,
                        'email'                     => $email,
                        'activation_code'           => $activation_code,
                        'forgotten_password_code'   => $forgotten_password_code,
                        'forgotten_password_time'   => $forgotten_password_time,
                        'remember_code'             => $remember_code,
                        'created_on'                => $created_on,
                        'last_login'                => $last_login,
                        'active'                    => $active,
                        'first_name'                => $first_name,
                        'last_name'                 => $last_name
                     
                     );
	
        $this->db->where('username', $username);
        $query = $this->db->get('users');
        $count_row1 = $query->num_rows();
        
         if ($count_row1 > 0) {
            
            return FALSE; 
            
        } else {
            
            $this->db->where('email', $email);
            $query = $this->db->get('users');
            $count_row2 = $query->num_rows();
        
            if ($count_row2 > 0) {
            
            return FALSE; 
            
            } else {
            
                $this->db->insert('users',$data);
                return TRUE; 
            }
            
        }
            
    }
    
    
    public function view_users($id)
    {
        $this->db->where('id', $id);
	$result=$this->db->get('users');;
	return $result->row_array();
    }
    
}

