<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Backend extends CI_Controller {
    
    
    
    public function index()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu1');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    public function dashboard()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu1');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    
    public function template1()
    {
        $this->load->view('backend/template');
    }
        
    public function template2()
    {
        $this->load->view('backend/template2');
    }
    
    public function menu()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }
    
    public function menu1()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu1');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }
     
    public function menu2()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu2');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }

    public function menu3()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu3');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    public function menu4()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu4');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }
     
    public function menu5()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu5');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }

    public function menu6()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu6');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    public function menu7()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu7');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }
     
    public function menu8()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu8');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }

    public function menu9()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu9');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    public function menu10()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu10');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }
     
    public function menu11()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu11');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
     }

    public function menu12()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/menu12');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    

    public function database_management()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/database_management');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
    public function parameters_management()
    {
        $this->load->view('backend/layout/header');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/parameters_management');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footer');
    }
    
 }


?>