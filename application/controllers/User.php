<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {

    //put your code here
    var $template = 'web/template';

    public function index()
    {
        $data['result'] = $this->user_model->get_user();
        
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/view_all', $data);
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
    }
    
    public function add()
    {
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/add_user_x');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
        
    }
    
    public function add_user()
    {
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/menu/add_user');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
        
        
         if (isset($_POST['submit']))
         {
         
            $this->form_validation->set_rules('last_name', 'last_name', 'required');
        
            if ($this->form_validation->run() == FALSE)
	    {
                 $this->load->view('backend/layout/headertable');
                 $this->load->view('backend/template/adminlte');
        
                 $this->load->view('backend/menu/add_user');
        
                 $this->load->view('backend/layout/copyright');
                 $this->load->view('backend/layout/sidebar');
                 $this->load->view('backend/layout/footertable');
        
            }
	    else 
	    {
                $first_name =$_POST['first_name'];
                $last_name  =$_POST['last_name'];
                
                
                $result = $this->user_model->add_user($first_name, $last_name);
                
                if($result==TRUE)
                {
                    
                    $this->load->view('backend/menu/success');
                }
                else
                {
                    
                    $this->load->view('backend/menu/failed');
                }
                
            }
            
            
         }
        
    }
    
    
    public function edit($id)
    {
        $data['result'] = $this->user_model->view_users($id);
        
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/edit_user', $data);
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
    }
    
    public function view($id)
    {
        $data['result'] = $this->user_model->view_users($id);
        
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/view_user', $data);
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
    }
    
    public function del($id)
    {
        $data['result'] = $this->user_model->view_users($id);
        
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/del_user', $data);
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
        
    }
    
    public function search()
    {
        
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/search');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
        
    }
    
    public function register()
    {
        
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/register');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
        
             		
				
		if (isset($_POST['submit']))
		{
			$this->form_validation->set_rules('first_name', 'first_name', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('backend/layout/headertable');
                                $this->load->view('backend/template/adminlte');
                                $this->load->view('backend/menu/register');
                                $this->load->view('backend/layout/copyright');
                                $this->load->view('backend/layout/sidebar');
                                $this->load->view('backend/layout/footertable');
			}
			else 
			{
                                $username   =$_POST['username'];
                                $password   =$_POST['password'];
                                $email      =$_POST['email'];
                                $created_on =time();
                                $first_name =$_POST['first_name'];
				$last_name  =$_POST['last_name'];
                                
                                $ip         = '127.0.0.1';
				
			        
                                $result= $this->user_model->add_user(
                                            $ip,
                                            $username,
                                            $password,
                                            $salt=NULL,
                                            $email,
                                            $activation_code=NULL,
                                            $forgotten_password_code=NULL,
                                            $forgotten_password_time=NULL,
                                            $remember_code=NULL,
                                            $created_on,
                                            $last_login=NULL,
                                            $active=NULL,
                                            $first_name, 
                                            $last_name,
                                            $company=NULL,   
                                            $phone=NULL
                                );
                                
                                
                                if($result==TRUE)
                                {
                                    redirect('user/success','refresh');
                                }
                                else 
                                {
                                    redirect('user/failed','refresh');
                                }
	
			}
	
		}
		
    }
    
    public function success()
    {
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/success');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
    }
    
    public function failed()
    {
        $this->load->view('backend/layout/headertable');
        $this->load->view('backend/template/adminlte');
        $this->load->view('backend/menu/failed');
        $this->load->view('backend/layout/copyright');
        $this->load->view('backend/layout/sidebar');
        $this->load->view('backend/layout/footertable');
    }
    
}